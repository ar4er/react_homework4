import ProductList from "../component/product-list/ProductList";
import { useSelector } from "react-redux";

export function Basket() {
  const products = useSelector(
    (state) => state.magazine.products.productsInBasket
  );
  return products.length ? (
    <ProductList page="basket" products={products} />
  ) : (
    "Корзина порожня ):"
  );
}
