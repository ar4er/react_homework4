import PropTypes from "prop-types";
import Product from "../product/Product";
import styles from "./ProductList.module.scss";

function ProductList({ page, products }) {
  return (
    <div className={styles.ProductList}>
      {products &&
        products.map((product) => (
          <Product product={product} page={page} key={product.article} />
        ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.array,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};

export default ProductList;
