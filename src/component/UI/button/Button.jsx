import PropTypes from "prop-types";
import classes from "./Button.module.scss";

function Button({ backgroundColor, onClick, text }) {
  return (
    <button
      className={classes.Button}
      style={{ background: backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}

Button.defaultProps = {
  backgroundColor: "yellow",
};

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};

export default Button;
