import { useEffect } from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductsArray } from "./redux/actions/magazine";
import styles from "./styles/index.module.scss";
import Modal from "./component/UI/modal/Modal";
import { Basket, Favorite, Home } from "./pages";
import favoriteIcon from "./image/favourites-star.png";
import basketIcon from "./image/basket.png";
import homeIcon from "./image/home.png";

const App = () => {
  const products = useSelector((state) => state.magazine.products);
  const productsInFavorite = products.productsInFavorite;
  const productsInBasket = products.productsInBasket;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsArray());
  }, [dispatch]);

  return (
    <div className={styles.App}>
      <nav className={styles.productCounts}>
        <NavLink to="/" className={styles.navigateIcon}>
          <img src={homeIcon} alt="Home" />
        </NavLink>
        <NavLink to="/favorites" className={styles.navigateIcon}>
          <img src={favoriteIcon} alt="Favorite" />
          <span className={styles.productCount}>
            {productsInFavorite.length}
          </span>
        </NavLink>
        <NavLink to="/basket" className={styles.navigateIcon}>
          <img src={basketIcon} alt="Basket" />
          <span className={styles.productCount}>{productsInBasket.length}</span>
        </NavLink>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="/favorites"
          element={<Favorite products={productsInFavorite} />}
        />
        <Route
          path="/basket"
          element={<Basket products={productsInBasket} />}
        />
      </Routes>
      <Modal />
    </div>
  );
};

export default App;
