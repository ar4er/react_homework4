import { magazineTypes } from "../types";

!localStorage.getItem("productsInFavorite") &&
  localStorage.setItem("productsInFavorite", JSON.stringify([]));

!localStorage.getItem("productsInBasket") &&
  localStorage.setItem("productsInBasket", JSON.stringify([]));

const initialState = {
  products: {
    products: [],
    productsInFavorite: JSON.parse(localStorage.getItem("productsInFavorite")),
    productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
  },
  modal: {
    visible: false,
    modalId: "default",
    submitFunction: null,
    data: null,
  },
};

const getProductsInFavorite = () =>
  JSON.parse(localStorage.getItem("productsInFavorite"));

const getProductsInBasket = () =>
  JSON.parse(localStorage.getItem("productsInBasket"));

export function magazineReducer(state = initialState, action) {
  switch (action.type) {
    case magazineTypes.GET_ALL_PRODUCTS:
      return {
        ...state,
        products: { ...state.products, products: action.payload.products },
      };
    case magazineTypes.ADD_PRODUCT_TO_FAVORITE:
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([...getProductsInFavorite(), action.payload.product])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInFavorite: JSON.parse(
            localStorage.getItem("productsInFavorite")
          ),
        },
      };
    case magazineTypes.REMOVE_PRODUCT_FROM_FAVORITE:
      const productInFavoriteIndex = getProductsInFavorite().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([
          ...getProductsInFavorite().slice(0, productInFavoriteIndex),
          ...getProductsInFavorite().slice(productInFavoriteIndex + 1),
        ])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInFavorite: JSON.parse(
            localStorage.getItem("productsInFavorite")
          ),
        },
      };
    case magazineTypes.ADD_PRODUCT_TO_BASKET:
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([...getProductsInBasket(), action.payload.product])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInBasket: JSON.parse(
            localStorage.getItem("productsInBasket")
          ),
        },
      };
    case magazineTypes.REMOVE_PRODUCT_FROM_BASKET:
      const productInBasketIndex = getProductsInBasket().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([
          ...getProductsInBasket().slice(0, productInBasketIndex),
          ...getProductsInBasket().slice(productInBasketIndex + 1),
        ])
      );
      return {
        ...state,
        products: {
          ...state.products,
          productsInBasket: JSON.parse(
            localStorage.getItem("productsInBasket")
          ),
        },
      };
    case magazineTypes.OPEN_MODAL:
      return {
        ...state,
        modal: {
          visible: true,
          modalId: action.payload.modalId,
          submitFunction: action.payload.submitFunction,
          data: action.payload.data,
        },
      };
    case magazineTypes.CLOSE_MODAL:
      return {
        ...state,
        modal: {
          visible: false,
          modalId: "default",
          submitFunction: null,
          data: null,
        },
      };
    default:
      return state;
  }
}
